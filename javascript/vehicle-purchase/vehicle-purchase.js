// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

export function needsLicense(kind) {
  return kind === 'car' || kind === 'truck';
}

export function chooseVehicle(option1, option2) {
  const betterOption = option1 > option2 ? option2 : option1;
  return `${betterOption} is clearly the better choice.`
}

export function calculateResellPrice(originalPrice, age) {
  return priceFactor(age) * originalPrice;
}

function priceFactor(age){
  switch (true){
    case age < 3:
      return 0.8;
    case age <= 10:
      return 0.7;
    default:
      return 0.5;
  }
}

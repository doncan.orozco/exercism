export function twoSum(array1, array2) {
  return joinNumbers(array1) + joinNumbers(array2)
}

function joinNumbers(array){
  return Number(array.join(''));
}

export function luckyNumber(value) {
  return String(value) === String(value).split('').reverse().join('');
}

export function errorMessage(input) {
  if (!input) return 'Required field'
  if (!Number(input)) return 'Must be a number besides 0'
  return ''
}
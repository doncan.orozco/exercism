// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

const JUICE_PREPARATION_TIMES = {
  'Pure Strawberry Joy': 0.5,
  'Energizer': 1.5,
  'Green Garden': 1.5,
  'Tropical Island': 3,
  'All or Nothing': 5
}

const DEFAULT_PREPARATION_TIME = 2.5;

const WEDGES_PER_LIME = {
  small: 6,
  medium: 8,
  large: 10
}

/**
 * Determines how long it takes to prepare a certain juice.
 *
 * @param {string} name
 * @returns {number} time in minutes
 */
export function timeToMixJuice(name) {
  return JUICE_PREPARATION_TIMES[name] || DEFAULT_PREPARATION_TIME;
}

/**
 * Calculates the number of limes that need to be cut
 * to reach a certain supply.
 *
 * @param {number} wedgesNeeded
 * @param {string[]} limes
 * @returns {number} number of limes cut
 */
export function limesToCut(wedgesNeeded, limes) {
  const limesCopy = [...limes];
  let wedges = 0;
  let numberOfLimes = 0;

  while (wedges < wedgesNeeded && limesCopy.length) { 
    wedges += WEDGES_PER_LIME[limesCopy.shift()];
    numberOfLimes += 1;
  }
  return numberOfLimes;
}

/**
 * Determines which juices still need to be prepared after the end of the shift.
 *
 * @param {number} timeLeft
 * @param {string[]} orders
 * @returns {string[]} remaining orders after the time is up
 */
export function remainingOrders(timeLeft, orders) {
  const ordersCopy = [...orders];
  while (timeLeft > 0) { timeLeft -= timeToMixJuice(ordersCopy.shift()) };
  return ordersCopy;
}
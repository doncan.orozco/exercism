// @ts-check
//
// The line above enables type checking for this file. Various IDEs interpret
// the @ts-check directive. It will give you helpful autocompletion when
// implementing this exercise.

const DAYS_IN_WEEK = 7;
/**
 * Calculates the total bird count.
 *
 * @param {number[]} birdsPerDay
 * @returns {number} total bird count
 */
export function totalBirdCount(birdsPerDay) {
  let birdCounter = 0;
  for (let i = 0; i < birdsPerDay.length; i++) {
    birdCounter += birdsPerDay[i];
  }
  return birdCounter;

  //return birdsPerDay.reduce((acu, birds) => acu + birds)
}

/**
 * Calculates the total number of birds seen in a specific week.
 *
 * @param {number[]} birdsPerDay
 * @param {number} week
 * @returns {number} birds counted in the given week
 */
export function birdsInWeek(birdsPerDay, week) {
  let offset = (week - 1) * DAYS_IN_WEEK
  let birdCounter = 0;
  for (let i = offset ; i < offset + DAYS_IN_WEEK; i++) {
    birdCounter += birdsPerDay[i];
  }
  return birdCounter;

  //return totalBirdCount(birdsPerDay.slice(offset, offset + DAYS_IN_WEEK))
}

/**
 * Fixes the counting mistake by increasing the bird count
 * by one for every second day.
 *
 * @param {number[]} birdsPerDay
 * @returns {number[]} corrected bird count data
 */
export function fixBirdCountLog(birdsPerDay) {
  for (let i = 0; i < birdsPerDay.length; i++) {
    if (i % 2 === 0){
      birdsPerDay[i] += 1 
    }
  }
  return birdsPerDay

  //return birdsPerDay.map((birds, index) => index % 2 === 0 ? birds + 1 : birds)
}
